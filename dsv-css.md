#DSV - CSS

## Getting started:
### Content:
* [Images](http://unsplash.it/ "Unsplash")
* [Placeholder/Text Block](http://meettheipsums.com/)
### Naming conventions
* use lowercase
* using dash to separate words
* avoid linking directly to file in other websites (third-party used to specifically hosting is fine)

## CSS core
### Terminology and Syntax:
* **Selectors**: determine HTML ele
* **Declaration blocks**: consist of style rule(s), enclosed in {}
* **Declaration**: style rule
* **Properties**: type of style
* **Value**: properties of style
* ### Resources:
	* [codrops](https://tympanus.net/codrops/)
	* Mozilla Development Network
* **Pseudo-class**: hover, link, visited, active, focus...
* **Descendant selectors**: avoid going more than 3 lvls deep
* **Combining selectors**: each selector is independent of each other
* ### Color resources:
	* [CCS colours name](http://colours.neilorangepeel.com/ "list all available keywords, hex, RGB")
	* [Coolors](https://coolors.co/ "generate color pallette")
	* [Random A11y](https://randoma11y.com "color combination")
* ### Cascading, inheritance, and specificity:
	* [Table of properties](https://www.w3.org/TR/CSS21/propidx.html "inheritance of properties")
	* Specificity ranking: ID > class > type
	* [Comparing ranking](http://specificity.keegan.st/)

## Typography:
* **Typography**: the study of the design and use of type for communication
* **Typeface**: a set of fonts, designed w/ common characteristics and composed of glyphs
* **Font**: invidual files that are part of a typeface
* ### Web fonts and google font
	* **internal**: *@font-face*: used to set the font name and link to the font files
		```
			@font-face{
				font-family;
				src format('woof'/'woof2');
			}
		```
	> more info: goo.gl/rZZEEe
	* **external**: 
		* [google font](https://fonts.google.com/ "free")
		* [typekit](https://typekit.com/fonts "paid")
* ### The font-size property:	
	*  **pixel (px)**: absolute value, default: 16px. Avoid using decimal.
	* **em**: relative (1em = inherited font size). Default = 16px
	* **rem**: similar to em, but only relative to root ele (<html>)
* ### Font-style and font-weight properties:
	* *font-weight*: [100,900]
	